<?php

namespace Komtesa\EventSourcing\Domain\Eventing;

class ProductWasPublished implements Event
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function id()
    {
        return $this->id;
    }
}