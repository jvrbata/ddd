<?php

namespace Komtesa\EventSourcing\Domain\Eventing;

class ProductSkuWasChanged implements Event
{
    private $id;

    private $sku;

    public function __construct($id, $sku)
    {
        $this->id  = $id;
        $this->sku = $sku;
    }

    public function sku()
    {
        return $this->sku;
    }

    public function id()
    {
        return $this->id;
    }
}