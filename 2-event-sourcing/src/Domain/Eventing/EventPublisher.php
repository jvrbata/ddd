<?php

namespace Komtesa\EventSourcing\Domain\Eventing;

abstract class EventPublisher
{
    public static function publish(Event $event)
    {
        echo "Event " . get_class($event) . " was published \n";
    }
}