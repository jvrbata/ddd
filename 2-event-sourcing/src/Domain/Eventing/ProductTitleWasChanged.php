<?php

namespace Komtesa\EventSourcing\Domain\Eventing;

class ProductTitleWasChanged implements Event
{
    private $id;

    private $title;

    public function __construct($id, $title)
    {
        $this->id    = $id;
        $this->title = $title;
    }

    public function title()
    {
        return $this->title;
    }

    public function id()
    {
        return $this->id;
    }
}