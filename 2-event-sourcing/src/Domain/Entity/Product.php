<?php

namespace Komtesa\EventSourcing\Domain\Entity;

use Komtesa\EventSourcing\Domain\Eventing\ProductSkuWasChanged;
use Komtesa\EventSourcing\Domain\Eventing\ProductTitleWasChanged;
use Komtesa\EventSourcing\Domain\Eventing\ProductWasExcluded;
use Komtesa\EventSourcing\Domain\Eventing\ProductWasPublished;

class Product extends AggregateRoot
{
    const UNPUBLISHED = 0;
    const PUBLISHED   = 1;
    const EXCLUDED    = 2;

    protected $id;

    protected $title;

    protected $sku;

    protected $state;

    public function __construct($id, $title, $sku)
    {
        parent::__construct();

        $this->id = $id;

        $this->changeTitle($title);
        $this->changeSku($sku);
        $this->publish();
    }

    public function id()
    {
        return $this->id;
    }

    public function title()
    {
        return $this->title;
    }

    public function sku()
    {
        return $this->sku;
    }

    public function state()
    {
        return $this->state;
    }

    public function changeTitle($title)
    {
        $this->handleEvent(new ProductTitleWasChanged($this->id, $title));
    }

    public function changeSku($sku)
    {
        $this->handleEvent(new ProductSkuWasChanged($this->id, $sku));
    }

    public function exclude()
    {
        $this->handleEvent(new ProductWasExcluded($this->id));
    }

    public function publish()
    {
        $this->handleEvent(new ProductWasPublished($this->id));
    }

    protected function applyProductTitleWasChanged(ProductTitleWasChanged $event)
    {
        $this->title = $event->title();
    }

    protected function applyProductSkuWasChanged(ProductSkuWasChanged $event)
    {
        $this->sku = $event->sku();
    }

    protected function applyProductWasExcluded(ProductWasExcluded $event)
    {
        $this->state = self::EXCLUDED;
    }

    protected function applyProductWasPublished(ProductWasPublished $event)
    {
        $this->state = self::PUBLISHED;
    }
}