<?php

namespace Komtesa\EventSourcing\Domain\Entity;

use Komtesa\EventSourcing\Domain\Eventing\Event;
use Komtesa\EventSourcing\Domain\Eventing\EventPublisher;

class AggregateRoot
{
    /** @type Event[] */
    protected $events;

    public function __construct()
    {
        $this->events = [];
    }

    protected function handleEvent(Event $event)
    {
        $this->recordEvent($event);
        $this->applyEvent($event);
        $this->publishEvent($event);
    }

    protected function recordEvent(Event $event)
    {
        $this->events[] = $event;
    }

    protected function applyEvent(Event $event)
    {
        $reflection = new \ReflectionClass($event);

        $modifier = 'apply' . ($reflection->getShortName());

        $this->$modifier($event);
    }

    protected function publishEvent(Event $event)
    {
        EventPublisher::publish($event);
    }

    public function events()
    {
        return $this->events;
    }

    public function clearEvents()
    {
        $this->events = [];
    }
}