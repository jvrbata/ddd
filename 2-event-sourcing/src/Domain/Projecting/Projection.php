<?php

namespace Komtesa\EventSourcing\Domain\Projecting;

use Komtesa\EventSourcing\Domain\Eventing\Event;

interface Projection
{
    public function listensTo();

    public function project(Event $event);
}