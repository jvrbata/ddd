<?php

namespace Komtesa\EventSourcing\Domain\Repository;

use Komtesa\EventSourcing\Domain\Entity\Product;

interface ProductRepository
{
    public function get($id);

    public function find($id);

    public function save(Product $product);

    public function remove($id);
}