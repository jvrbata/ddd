<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Mapping;

use Komtesa\EventSourcing\Domain\Entity\Product;

class ProductDatabaseDriverMapper
{
    public function mapFrom(\stdClass $dataObject)
    {
        $classReflection = new \ReflectionClass(Product::class);
        $product         = $classReflection->newInstanceWithoutConstructor();

        foreach (['id', 'title', 'sku', 'state'] as $property) {
            $propertyReflection = $classReflection->getProperty($property);
            $propertyReflection->setAccessible(true);
            $propertyReflection->setValue($product, $dataObject->$property);
        }

        return $product;
    }

    public function mapToDataObject(Product $product)
    {
        $dataObject = new \stdClass();

        $dataObject->id    = $product->id();
        $dataObject->title = $product->title();
        $dataObject->sku   = $product->sku();
        $dataObject->state = $product->state();

        return $dataObject;
    }
}