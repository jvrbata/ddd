<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Repository;

use Komtesa\EventSourcing\Domain\Entity\Product;
use Komtesa\EventSourcing\Domain\Repository\ProductRepository as DomainProductRepository;
use Komtesa\EventSourcing\Infrastructure\Joomla\Mapping\ProductDatabaseDriverMapper;
use Komtesa\EventSourcing\Infrastructure\Joomla\Mapping\ProductMapper;
use Komtesa\EventSourcing\Infrastructure\Joomla\Projecting\Projector;

class ProductRepository implements DomainProductRepository
{
    /** @type  Projector */
    private $projector;

    /** @type ProductMapper */
    private $mapper;

    public function __construct(ProductDatabaseDriverMapper $mapper, Projector $projector)
    {
        $this->projector = $projector;
        $this->mapper    = $mapper;
    }

    public function find($id)
    {
       $query = new FindByIdQuery($id);

       $result = $query->execute();


        if (!$result) {
            throw new \Exception("Product with id {$id} was not found");
        }

        return $this->mapper->mapFrom($result);
    }

    public function save(Product $product)
    {
        $dataObject = $this->mapper->mapToDataObject($product);

        $query = new SaveProductQuery($dataObject);
        $query->execute();

        $this->projector->project($product->events());

        $product->clearEvents();
    }

    public function remove($id)
    {
        $this->mapper->remove($id);
    }
}