<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying;

use Joomla\Database\DatabaseDriver;

class UpdateProduct
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute(\stdClass $product)
    {
        $query = $this->driver->getQuery(true);

        $query->update('products')
            ->set([
                'title = ' . $query->quote($product->title),
                'sku = ' . $query->quote($product->sku),
                'state = ' . (int)$product->state,
            ])
            ->where('id = ' . (int)$product->id);

        $this->driver->setQuery($query)->execute();
    }
}