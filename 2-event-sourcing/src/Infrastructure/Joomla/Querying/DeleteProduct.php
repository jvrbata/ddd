<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying;

use Joomla\Database\DatabaseDriver;

class DeleteProduct
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute($id)
    {
        $query = $this->driver->getQuery(true);
        $query->delete('products')
            ->where('id = ' . (int)$id);

        $this->driver->setQuery($query)
            ->execute();
    }
}