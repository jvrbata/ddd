<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying;

use Joomla\Database\DatabaseDriver;

class FindProductById
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute($id)
    {
        $query = $this->driver->getQuery(true);
        $query->select('id, title, sku, state')
            ->from('products')
            ->where('id = ' . (int)$id);

        $this->driver->setQuery($query);

        return $this->driver->loadObject();
    }
}