<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying;

use Joomla\Database\DatabaseDriver;

class InsertProduct
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute(\stdClass $product)
    {
        $query = $this->driver->getQuery(true);

        $query->insert('products')
            ->columns(['id', 'title', 'sku', 'state'])
            ->values(implode(',', [
                (int)$product->id,
                $query->quote($product->title),
                $query->quote($product->sku),
                (int)$product->state
            ]));

        $this->driver->setQuery($query)->execute();
    }
}