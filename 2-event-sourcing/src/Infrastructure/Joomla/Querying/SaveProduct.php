<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying;

use Joomla\Database\DatabaseDriver;

class SaveProduct
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute(\stdClass $product)
    {
        if ($this->exists($product)) {
            $this->update($product);

            return;
        }

        $this->insert($product);
    }

    private function exists(\stdClass $product)
    {
        $query   = new FindProductById($this->driver);
        $product = $query->execute($product->id);

        return $product !== null;
    }

    private function update(\stdClass $product)
    {
        $query = new UpdateProduct($this->driver);
        $query->execute($product);
    }

    private function insert(\stdClass $product)
    {
        $query = new InsertProduct($this->driver);
        $query->execute($product);
    }
}