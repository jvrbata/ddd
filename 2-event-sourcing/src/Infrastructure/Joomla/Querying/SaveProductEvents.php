<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying;

use Joomla\Database\DatabaseDriver;

class SaveProductEvents
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute(array $events)
    {
        $query = $this->driver->getQuery(true);
        $query->insert('product_events')
            ->columns(['product_id', 'event'])
            ->values(array_map(function ($event) use ($query) {
                return implode(',', [
                    (int)$event->id(),
                    $query->quote(serialize($event))
                ]);
            }, $events));

        $this->driver->setQuery($query)
            ->execute();
    }
}