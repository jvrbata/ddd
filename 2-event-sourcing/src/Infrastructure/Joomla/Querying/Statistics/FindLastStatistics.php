<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying\Statistics;

use Joomla\Database\DatabaseDriver;

class FindLastStatistics
{
    /** @type  DatabaseDriver */
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute()
    {
        $query = $this->driver->getQuery(true);
        $query->select('published_count as publishedCount, excluded_count as excludedCount, date')
            ->from('products_statistics')
            ->order('ordering DESC');

        $this->driver->setQuery($query);

        $dataObject = $this->driver->loadObject();

        if (!$dataObject) {
            return null;
        }

        $dataObject->date = \DateTime::createFromFormat('Y-m-d H:i:s', $dataObject->date);

        return $dataObject;
    }
}