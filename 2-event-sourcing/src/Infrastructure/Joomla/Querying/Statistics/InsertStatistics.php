<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Querying\Statistics;

use Joomla\Database\DatabaseDriver;

class InsertStatistics
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function execute(\stdClass $statistics)
    {
        $query = $this->driver->getQuery(true);

        $query->insert('products_statistics')
            ->columns(['published_count', 'excluded_count', 'date'])
            ->values(implode(',', [
                (int)$statistics->publishedCount,
                (int)$statistics->excludedCount,
                $query->quote($statistics->date->format('Y-m-d H:i:s'))
            ]));

        $this->driver->setQuery($query)->execute();
    }
}