<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Projecting;

use Komtesa\EventSourcing\Domain\Eventing\Event;
use Komtesa\EventSourcing\Projecting\Projection;

class Projector
{
    private $projections;

    public function __construct()
    {
        $this->projections = [];
    }

    /**
     * @param Projection[] $projections
     */
    public function register(array $projections)
    {
        foreach ($projections as $projection) {
            $this->projections[$projection->listensTo()][] = $projection;
        }
    }

    /**
     * @param Event[] $events
     */
    public function project(array $events)
    {
        foreach ($events as $event) {
            $this->projectEvent($event);
        }
    }

    private function projectEvent(Event $event)
    {
        $eventType = get_class($event);

        if (!array_key_exists($eventType, $this->projections)) {
            return;
        }

        /** @type Projection $projection */
        foreach ($this->projections[$eventType] as $projection) {
            $projection->project($event);
        }
    }
}