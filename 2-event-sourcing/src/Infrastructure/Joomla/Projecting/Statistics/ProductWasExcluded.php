<?php

namespace Komtesa\EventSourcing\Infrastructure\Joomla\Projecting\Statistics;

use Joomla\Database\DatabaseDriver;
use Komtesa\EventSourcing\Domain\Eventing\Event;
use Komtesa\EventSourcing\Infrastructure\Joomla\Querying\Statistics\FindLastStatistics;
use Komtesa\EventSourcing\Infrastructure\Joomla\Querying\Statistics\InsertStatistics;
use Komtesa\EventSourcing\Domain\Projecting\Projection;

class ProductWasExcluded implements Projection
{
    private $driver;

    public function __construct(DatabaseDriver $driver)
    {
        $this->driver = $driver;
    }

    public function listensTo()
    {
        return \Komtesa\EventSourcing\Domain\Eventing\ProductWasExcluded::class;
    }

    public function project(Event $event)
    {
        $this->driver->transactionStart();

        $findLastStatisticsQuery = new FindLastStatistics($this->driver);
        $lastStatistics          = $findLastStatisticsQuery->execute();

        if (!$lastStatistics) {
            throw new \Exception('Products statistics were not found');
        }

        $lastStatistics->publishedCount && $lastStatistics->publishedCount--;
        $lastStatistics->excludedCount++;

        $insertStatisticsQuery = new InsertStatistics($this->driver);

        $insertStatisticsQuery->execute($lastStatistics);

        $this->driver->transactionCommit();
    }
}