Domain / Infrastructure / Application layers
Hexagonal architecture
Aggregate root
Domain events - immutable data structures
Joomla plugins / evens (hooks) vs Domain events
Event sourcing
CQRS - read / write models
Projectors / Domain events
Repository / Data mapper / Data transfer objects
Query objects