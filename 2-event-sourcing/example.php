<?php

require_once __DIR__ . '/vendor/autoload.php';

$databaseFactory = new \Joomla\Database\DatabaseFactory();
$driver          = $databaseFactory->getDriver('mysqli', [
    'host'     => 'localhost',
    'user'     => 'root',
    'password' => '',
    'database' => 'event-sourcing'
]);

foreach (['products_statistics', 'product_events', 'products'] as $table) {
    $query = "TRUNCATE {$driver->quoteName($table)}";
    $driver->setQuery($query)->execute();
}

/***************************************************************************************************/


$projector = new \Komtesa\EventSourcing\Infrastructure\Joomla\Projecting\Projector();

$projections = [
    new \Komtesa\EventSourcing\Infrastructure\Joomla\Projecting\Statistics\ProductWasExcluded($driver),
    new \Komtesa\EventSourcing\Infrastructure\Joomla\Projecting\Statistics\ProductWasPublished($driver)
];

$projector->register($projections);

$productMapper     = new \Komtesa\EventSourcing\Infrastructure\Joomla\Mapping\ProductDatabaseDriverMapper($driver);
$productRepository = new \Komtesa\EventSourcing\Infrastructure\Joomla\Repository\ProductRepository($productMapper, $projector);

$productRepository->remove(1);
$productRepository->remove(2);

$lg   = new \Komtesa\EventSourcing\Domain\Entity\Product(1, 'LG G2', 'lg-g2');
$asus = new \Komtesa\EventSourcing\Domain\Entity\Product(2, 'Asus N750jv', 'asus-n750jv');

$productRepository->save($asus);
$productRepository->save($lg);

$asus->exclude();
$productRepository->save($asus);

$lg->exclude();
$lg->publish();
$productRepository->save($lg);

$loadedProduct = $productRepository->get(1);

var_dump($loadedProduct);

