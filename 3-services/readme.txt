Services
- stateless
- application services - transfers data to domain model, framework agnostic
- domain services - operations cross multiple aggregate roots
- infrastructure services - logging data, sending e-mails - outside of domain model (domain event - notify customer, infrastructure - handle request)

Application uses domain, infrastructure implements domain