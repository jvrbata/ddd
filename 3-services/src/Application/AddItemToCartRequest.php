<?php

namespace Komtesa\Application\Service;

class AddItemToCartRequest
{
    private $product;

    private $quantity;

    public function __construct($product, $quantity)
    {
        $this->product  = $product;
        $this->quantity = $quantity;
    }

    public function product()
    {
        return $this->product;
    }

    public function quantity()
    {
        return $this->quantity;
    }
}