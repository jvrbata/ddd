<?php

namespace Komtesa\Application\Service;

class Cart
{
    private $id;

    private $events;

    public function add(CartItem $item)
    {
        $this->events[] = new ItemWasAddedToCart($this->id, $item->id);
    }
}