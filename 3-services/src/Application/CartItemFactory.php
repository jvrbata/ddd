<?php

namespace Komtesa\Application\Service;

class CartItemFactory
{
    /**
     * @param \Komtesa\Application\Service\DataObject $data
     *
     * @return \Komtesa\Application\Service\CartItem
     */
    public function createCartItem(DataObject $data)
    {
        return new CartItem();
    }
}