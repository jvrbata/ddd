<?php

namespace Komtesa\Application\Service;

class CartRepository
{
    /**
     * @param \Komtesa\Application\Service\User $user
     *
     * @return \Komtesa\Application\Service\Cart
     */
    public function findFor(User $user)
    {
        return new Cart();
    }

    public function save($cart)
    {
    }
}