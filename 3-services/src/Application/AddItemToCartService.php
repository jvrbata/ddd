<?php

namespace Komtesa\Application\Service;

class AddItemToCartService
{
    /** @type \Komtesa\Application\Service\CartRepository */
    private $repository;
    
    /**
     * @type \Komtesa\Application\Service\CartItemFactory
     */
    private $factory;

    public function __construct(CartRepository $repository, CartItemFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function execute(User $user, AddItemToCartRequest $request)
    {
        $cart = $this->repository->findFor($user);
        $cart->add($this->createCartItem($request));

        $this->repository->save($cart);
    }

    /**
     * @param \Komtesa\Application\Service\AddItemToCartRequest $request
     *
     * @return \Komtesa\Application\Service\CartItem
     */
    private function createCartItem(AddItemToCartRequest $request)
    {
        return $this->factory->createCartItem(new DataObject($request->product(), $request->quantity()));
    }
}