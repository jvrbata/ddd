<?php

namespace Komtesa\Domain\Service;

use Komtesa\Application\Service\Product;

interface PreOrderProduct
{
    public function execute(Product $product);
}