- each bounded context = application
- namespaces should be named in terms of Ubiquitous Language
- not based on patterns or building blocks (Value Objects, Services, Entities, etc.).
- infrastructure inside - possible coupling to framework

Sylius
https://github.com/Sylius/Sylius/tree/master/src/Sylius/Component

Elcodi
https://github.com/elcodi/elcodi/tree/master/src/Elcodi/Component

Sylius - better structure than elcodi