<?php

namespace Komtesa\Domain\Validating;

abstract class Validator
{
    /** @type \Komtesa\Domain\Validating\Handler */
    protected $handler;

    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
    }

    protected function handle($error)
    {
        $this->handler->handle($error);
    }

    abstract public function validate();
}