<?php

namespace Komtesa\Domain\Validating;

interface Handler
{
    public function handle($error);
}