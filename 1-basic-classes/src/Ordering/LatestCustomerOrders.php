<?php

namespace Komtesa\Ordering;


class LatestCustomerOrders
{
    /** @type \Komtesa\Ordering\OrderRepository */
    private $repository;

    /** @type \Komtesa\Ordering\OrderSpecificationFactory */
    private $factory;

    public function __construct(OrderRepository $repository, OrderSpecificationFactory $factory) //LatestCustomerOrdersSpecificationFactory
    {
        $this->repository = $repository;
        $this->factory    = $factory;
    }

    public function execute(Customer $customer)
    {
        return $this->repository->query($this->factory->createLatestCustomerOrders($customer)); // in-memory, Joomla, Doctrine, Redis...
    }
}