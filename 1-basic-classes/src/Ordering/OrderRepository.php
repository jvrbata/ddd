<?php

namespace Komtesa\Ordering;

interface OrderRepository
{
}

class JDatabaseDriverOrderRepository implements OrderRepository
{
    public function query(JDatabaseDriverSpecification $specification)
    {
        return new Order();
    }
}

class DoctrineOrderRepository implements OrderRepository
{
    public function query(DoctrineSpecification $specification)
    {
        return new Order();
    }
}