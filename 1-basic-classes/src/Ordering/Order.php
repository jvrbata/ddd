<?php

namespace Komtesa\Ordering;


class Order
{
    private $items;

    public function addItem($title, $sku, $price, $quantity)
    {
        $this->items[] = new Item($title, $sku, $price, $quantity);
    }
}