<?php

namespace Komtesa\Ordering;

class OrderSpecificationFactory
{
    /**
     * @param \Komtesa\Ordering\Customer $customer
     *
     * @return \Komtesa\Ordering\Specification
     */
    public function createLatestCustomerOrders(Customer $customer)
    {
        return new Specification();
    }
}