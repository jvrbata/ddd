<?php

namespace Komtesa\Domain\Pricing;

class Currency
{
    private $title;

    private $code;

    public function __construct(string $title, string $code)
    {
        $this->title = $title;
        $this->code  = $code;
    }

    public function code()
    {
        return $this->code;
    }

    public function isEqualTo(Currency $currency)
    {
        return $this->code() === $currency->code();
    }
}