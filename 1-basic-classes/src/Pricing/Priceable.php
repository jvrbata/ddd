<?php

namespace Komtesa\Domain\Pricing;

interface Priceable
{
    public function amount();

    public function currency();
}