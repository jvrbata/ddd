<?php

namespace Komtesa\Domain\Pricing;

class Price implements Priceable
{
    private $amount;

    private $currency;

    public function __construct($amount, Currency $currency)
    {
        $this->amount   = $amount;
        $this->currency = $currency;
    }

    public function isEqualTo(Price $price)
    {
        return $this->amount() === $price->amount() && $this->currency->isEqualTo($price->currency());
    }

    public function currency()
    {
        return $this->currency;
    }

    public function amount()
    {
        return $this->amount;
    }

    public function addAmount($amount)
    {
        return new self($this->amount() + $amount, $this->currency());
    }
}