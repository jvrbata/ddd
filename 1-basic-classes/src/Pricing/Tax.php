<?php

namespace Komtesa\Domain\Pricing;

class Tax
{
    private $rate;

    public function __construct($rate)
    {
        $this->rate = $rate;
    }

    public function rate()
    {
        return $this->rate;
    }
}