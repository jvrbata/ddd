<?php

namespace Komtesa\Domain\Pricing;

class PriceWithVat implements Priceable
{
    /** @type \Komtesa\Domain\Pricing\Price */
    private $price;

    /** @type \Komtesa\Domain\Pricing\Tax */
    private $tax;

    public function __construct(Price $price, Tax $tax)
    {
        $this->price = $price;
        $this->tax   = $tax;
    }

    public function amount()
    {
        return $this->price->amount() + $this->price->amount() * $this->tax->rate();
    }

    public function currency()
    {
        $this->price->currency();
    }

    public static function toPrice($amount, Currency $currency, Tax $tax)
    {
        $coefficient = $tax->rate() / (1 + $tax->rate());

        return new Price($amount * $coefficient, $currency);

    }
}