<?php

namespace Komtesa\Domain\Catalogue;

class Sku implements Identifier
{
    private $sku;

    public function __construct(string $sku)
    {
        $this->sku = $sku;
    }
}