<?php

namespace Komtesa\Domain\Catalogue;

use Komtesa\Domain\Pricing\PriceWithVat;

class Product implements Identifier
{
    /** @type \Komtesa\Domain\Catalogue\Uuid */
    private $id;

    /** @type \Komtesa\Domain\Catalogue\Title */
    private $title;

    /** @type \Komtesa\Domain\Catalogue\Sku */
    private $sku;

    /** @type \Komtesa\Domain\Catalogue\Ean */
    private $ean;

    /** @type \Komtesa\Domain\Pricing\PriceWithVat */
    private $price;

    public function __construct(Title $title, Sku $sku, Ean $ean, PriceWithVat $price)
    {
        $this->id    = Uuid::generate();
        $this->title = $title;
        $this->sku   = $sku;
        $this->ean   = $ean;
        $this->price = $price;
    }

    /**
     * @return \Komtesa\Domain\Pricing\PriceWithVat
     */
    public function price()
    {
        return $this->price;
    }

    public function validate(ProductValidationHandler $handler)
    {
        $validator = new ProductValidator($this, $handler);
        $validator->validate();
    }
}