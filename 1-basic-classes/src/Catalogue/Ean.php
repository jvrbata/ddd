<?php

namespace Komtesa\Domain\Catalogue;

class Ean implements Identifier
{
    private $ean;

    public function __construct(string $ean)
    {
        $this->ean = $ean;
    }
}