<?php

namespace Komtesa\Domain\Catalogue;

use Komtesa\Domain\Catalogue\Exceptions\TitleIsEmpty;
use Komtesa\Domain\Catalogue\Exceptions\TitleIsTooLong;

class Title
{
    private $title;

    public function __construct(string $title)
    {
        $this->validate($title);

        $this->title = $title;
    }

    private function validate(string $title)
    {
        if (strlen($title) > 255) {
            throw new TitleIsTooLong();
        }

        if (strlen($title) === 0) {
            throw new TitleIsEmpty();
        }
    }
}