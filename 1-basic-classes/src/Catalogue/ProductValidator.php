<?php

namespace Komtesa\Domain\Catalogue;

use Komtesa\Domain\Validating\Validator;

class ProductValidator extends Validator
{
    /** @type Product */
    private $product;

    /** @type  ProductValidationHandler */
    protected $handler;

    public function __construct(Product $product, ProductValidationHandler $handler)
    {
        parent::__construct($handler);

        $this->product = $product;
    }

    public function validate()
    {
        if ($this->product->price()->amount() === 0) {
            $this->handler->handleZeroPrice();
        }
    }
}