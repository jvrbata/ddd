<?php

namespace Komtesa\Domain\Catalogue\Exceptions;

class TitleIsTooLong extends CatalogueException
{
}