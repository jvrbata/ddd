<?php

namespace Komtesa\Domain\Catalogue;

class Uuid implements Identifier
{
    private $uuid;

    private function __construct($uuid)
    {
        $this->uuid = $uuid;
    }

    public static function generate()
    {
        return new self(uniqid());
    }

    public function isEqualTo(Uuid $uuid)
    {
        return $this->uuid() === $uuid->uuid();
    }

    public function uuid()
    {
        return $this->uuid;
    }
}