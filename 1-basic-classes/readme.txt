ValueObjects
- self validating
- always valid
- immutable
- equality
- side-effect-free behavior (price - add amount => immutable structure)

Entities
- generated identifier UUID4 (id as autoincrement = coupling domain model to persistence layer)
- multiple identifiers for same entity (uuid, sku, ean - single query method in repository)
- validating - preconditions, postconditions, invariants
    (preconditions - value objects - should not occur - validation of form != validation of domain objects)
    (postconditions - not validated in application layer - collector patter to notify application layer)
    (invariants => user can make only X orders per day -> validation in service layer)
- events (2-event-sourcing)

Aggregates
- Product + images + ...
- Order + items
- operations via aggregate root, loaded and stored in transaction
- referencing another objects != aggregates -> Product - Category - Manufacturer

Factory
- Factory Method on Aggregate Root

Builder
- complicated Aggregates / Entities, encapsulate validation